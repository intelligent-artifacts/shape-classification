# Shape Classification Tutorials #

## Description ##

The current repository contains two versions of the same shape classification tutorial. One uses the backtesting platform that is built into the Genie/GAIuS framework that simplifies the process of training and testing a GAIuS agent. The second tutorial notebook demonstrates the manual process used to train and test the agent without the use of the backtester.
